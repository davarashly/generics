public class Task1<T> {
    public static void main(String[] args) {
        print(9.0);
    }


    static <T> void print(T param) {
        if (param instanceof Integer || param instanceof Double || param instanceof String)
            System.out.println(param.getClass().getSimpleName() + " " + param);
        else
            System.out.println("Invalid type.");
    }
}