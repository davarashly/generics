/*
Создать функцию, в которую передаются два значения, каждое из которых
может являться либо целочисленным, либо вещественным, после чего
определяется тип, и функция возвращает их сумму либо в виде целочисленного,
либо в виде вещественного значения.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task3<T> {
    public static <T> void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите первое число");
        T m = (T) init(in.readLine());

        System.out.println("Введите второе число");
        T n = (T) init(in.readLine());
        System.out.println("\n" + m + " + " + n + " = " + sum(m, n));
        System.out.println(m + " * " + n + " = " + mult(m, n));
    }

    public static <T> T init(T param) {
        if (isInt((String) param))
            return (T) Integer.valueOf((String) param);
        else if (isDouble((String) param))
            return (T) Double.valueOf((String) param);
        else return null;
    }

    public static boolean isDouble(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isInt(String str) {
        try {
            int d = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static <T> T sum(T param1, T param2) {
        if (param1 instanceof Integer && param2 instanceof Integer) {
            Integer sum = (Integer) param1 + (Integer) param2;
            return (T) sum;
        } else if (param1 instanceof Double && param2 instanceof Double) {
            Double sum = (Double) param1 + (Double) param2;
            return (T) sum;
        } else if (param1 instanceof Integer && param2 instanceof Double) {
            Double sum = (Integer) param1 + (Double) param2;
            return (T) sum;
        } else if (param1 instanceof Double && param2 instanceof Integer) {
            Double sum = (Double) param1 + (Integer) param2;
            return (T) sum;
        } else return null;
    }

    public static <T> T mult(T param1, T param2) {
        if (param1 instanceof Integer && param2 instanceof Integer) {
            Integer mult = (Integer) param1 * (Integer) param2;
            return (T) mult;
        } else if (param1 instanceof Double && param2 instanceof Double) {
            Double mult = (Double) param1 * (Double) param2;
            return (T) mult;
        } else if (param1 instanceof Integer && param2 instanceof Double) {
            Double mult = (Integer) param1 * (Double) param2;
            return (T) mult;
        } else if (param1 instanceof Double && param2 instanceof Integer) {
            Double mult = (Double) param1 * (Integer) param2;
            return (T) mult;
        } else return null;
    }
}