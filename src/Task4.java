public class Task4<T> {
    public static void main(String[] args) {
        System.out.println(func("A"));
    }

    public static <T> int func(T param) {
        int sum = 0;
        if (param instanceof Character) {
            sum = (Integer) param;
        } else if (param instanceof String) {
            char[] chars = ((String) param).toCharArray();
            for (int i = 0; i < chars.length; i++) {
                sum += (int) chars[i];
            }
        }
        return sum;
    }
}
