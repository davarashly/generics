/*
            Создать функцию, в которую передается массив логических, целочисленных, либо вещественных значений,
            после чего распознается тип данных в массиве и уже в пределах функции создается массив с полученным типом,
            в который записываются переданные в функцию значения
*/
public class Task5 {

    public static void main(String[] args) {
        function(3, 4, 5, 6);
    }

    static <T> T[] function(T... array) {

        T[] res = null;

        if (array instanceof Integer[]) {
            Integer[] newArray = new Integer[array.length];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = (Integer) array[i];
            }
            res = (T[]) newArray;
        } else if (array instanceof Double[]) {
            Double[] newArray = new Double[array.length];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = (Double) array[i];
            }
            res = (T[]) newArray;
        } else if (array instanceof Boolean[]) {
            Boolean[] newArray = new Boolean[array.length];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = (Boolean) array[i];
            }
            res = (T[]) newArray;
        }
        return res;
    }
}
